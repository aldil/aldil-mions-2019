## 2) Qu'y-a-t-il derrière un logiciel ?


### C'est quoi un logiciel ? (ou un programme)

 - exécution d'une série d'instructions qui sera interprétée par une machine
 - cette série d'instructions = algorithme = code source


### Faisons notre premier algorithme&nbsp;!

(ne partez pas tout de suite !)


### Algorithme : préparer un café soluble

1. Se rapprocher de la bouilloire <!-- .element class="fragment"-->
1. Verser de l'eau dans la bouilloire <!-- .element class="fragment"-->
1. Activer la bouilloire <!-- .element class="fragment"-->
1. Prendre une tasse propre <!-- .element class="fragment"-->
1. Verser du café dans la tasse <!-- .element class="fragment"-->
1. Attendre l'extinction automatique de la bouilloire <!-- .element class="fragment"-->
1. Verser l'eau chaude dans la tasse <!-- .element class="fragment"-->


### Simple ?

Quelques bugs :

1. État préalable de la bouilloire ? Branchée, vide, à nettoyer ? <!-- .element class="fragment"-->
1. Quelle eau verse-t-on ? Eau gazeuse ? L'eau des toilettes ? <!-- .element class="fragment"-->
1. Quelle quantité de café mettre ? 12 cuillères ? <!-- .element class="fragment"-->
1. Que se passe-t-il si on n'a plus de tasse propre ? <!-- .element class="fragment"-->
1. … <!-- .element class="fragment"-->

Plus facile à dire qu'à faire donc ! <!-- .element class="fragment"-->


### Pas simple !

Plein de cas à gérer devant une tâche simple. C'est un travail d’équipe :

 - Programmation (écriture du code source / algorithme)
 - Report de bug (parce que les programmeurs sont des êtres imparfaits)
 - Retours utilisateurs
 - Design / ergonomie
 - Revue par les pairs
 - …


<!-- .slide: data-background="./images/recette.png" data-background-size="80%" -->
### Analogie avec la cuisine !

Qu'est-ce que c'est ?


<!-- .slide: data-background="./images/taboulet.jpg" data-background-size="70%" -->
### Différence entre code source et programme ?

La __recette de cuisine__ est au __code source__ ce que l'__exécution__ de la recette est au __programme__.


### Fin de cette partie

Vous venez de voir ce qu'étaient un algorithme et un programme !
