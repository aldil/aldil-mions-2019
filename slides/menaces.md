## 1) Un monde menaçant ?


### Les ordinateurs chez moi en 1999

<img src="images/Desktop.jpg" alt="ordinateur">


### Mes ordinateurs en 2019

<img src="images/home-office-336378_640.jpg" alt="ordinateur et smartphone" width="300">
<img src="images/book-3610618_640.jpg" alt="liseuse" width="300">

<img src="images/tablet-791050_640.jpg" alt="tablette" width="300">
<img src="images/car-dashboard-2667434_640.jpg" alt="ordinateur de voiture" width="300">


### Ils savent tout sur moi

<img src="images/drive.svg" alt="cozycloud" width="800">


### Qui contrôle qui ?


#### Refus de fonctionner
<!-- <img src="images/limite-atteinte.png" alt="Limite atteinte" width="600"> -->
<img src="images/desole-abonnement.png" alt="MS-Word arbitrairement désactivé" width="600">

Propriétaire du logiciel ?


#### Interdiction de passer la pub
<img src="images/blueray.png" alt="Blue-Ray" width="800">

Propriétaire du DVD ? Du lecteur ?


#### Surveillance constante
<img src="images/kindle-cafteur.png" alt="Amazon">

Droit à l'intimité ?


#### Interventions arbitraires
<img src="images/kindle-orwell.png" alt="Amazon">

Propriétaire de vos livres ?
