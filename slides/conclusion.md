## Conclusion


### C'est compliqué ?

Pas de souci, il y a des assos pour vous aider !


<!-- .slide: data-background="./images/aldil.png" data-background-size="50%" -->
### Trouvez votre GULL [1]

Pour obtenir de l'entraide, comme l'ALDIL à Lyon

<sub>[1] Groupe d'utilisateur et d'utilisatrice de logiciels libres</sub>

<sub><https://aldil.org></sub>


<!-- .slide: data-background="./images/lalis.png" data-background-size="50%" -->
### Formez-vous

Via LALIS

<sub><https://lalis.fr></sub>


<!-- .slide: data-background="./images/illyse.png" data-background-size="50%" -->
### Un Fournisseur d'Accès Internet associatif et militant

<sub><https://illyse.net></sub>

<sub><https://ffdn.net></sub>


<!-- .slide: data-background="./images/chatons.png" data-background-size="50%" -->
### Un CHATONS

Hébergeur associatif, comme Hadoly sur Lyon

<sub><https://hadoly.fr></sub>


### Soutenez des associations

En plus de celles citées précédemment :
 - Framasoft
 - La Quadrature du Net
 - Wikimedia France
 - La *Document Foundation* (Libre Office)
 - …


### Merci !
