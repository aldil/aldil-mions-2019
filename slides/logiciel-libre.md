<!-- .slide: data-background="./images/gnu.jpg" data-background-size="50%" -->
## 3) Le logiciel libre ?


<!-- .slide: data-background="./images/imprimante-stallman.png" data-background-size="70%" -->
### Son histoire

Une imprimante et un programmeur en 1983.


### Ses principes

 - La transparence<!-- .element class="fragment" -->
 - L'appropriation<!-- .element class="fragment" -->
 - Le partage<!-- .element class="fragment" -->
 - La collaboration<!-- .element class="fragment" -->


### Vos libertés

Ce n'est pas le logiciel qui est libre, ni son auteur / autrice, c'est elle/lui et vous !


### Liberté 0 : l'exécution du programme

Dérouler la recette de cuisine

Sans condition (important !) <!-- .element class="fragment" -->


### Liberté 1 : Étudier son fonctionnement

Connaître la composition du plat, les instructions.


### Liberté 2 : Modifier, améliorer, personnaliser

Personnaliser le plat librement.


### Liberté 3 : Redistribuer

Partager la recette d'origine ou modifiée.


### Quelques exemples de logiciels libres ?


<!-- .slide: data-background="./images/firefox.png" data-background-size="70%" -->
### Mozilla Firefox

Qui vous sert à naviguer sur internet

Alternative au logiciels privateurs : Chrome, Edge, Safari, …


<!-- .slide: data-background="./images/VLC.png" data-background-size="70%" -->
### VLC

Qui vous sert à lire des vidéos et de la musique (et bien plus).

Alternative au logiciel privateur : Windows Media Player


<!-- .slide: data-background="./images/Libre_office.png" data-background-size="70%" -->
### Libre Office

Qui vous sert à éditer des documents textes, tableurs, présentations, …

Alternative au logiciel privateur : Microsoft Office


<!-- .slide: data-background="./images/Tux.svg" data-background-size="40%" -->
### GNU / Linux

Une alternative libre à Microsoft Windows.


<!-- .slide: data-background="./images/fdroid.jpg" data-background-size="80%" -->
### Sur mobile ?

F-Droid : magasin d'applications libre sous Android.


### Et des services alternatifs à ce que j'utilise ?

Des services en ligne :

 - <span style="color: red">Youtube</span> → <span style="color: #07ed07">Peertube</span>
 - <span style="color: red">Facebook</span> → <span style="color: #07ed07">Diaspora</span>
 - <span style="color: red">Twitter</span> → <span style="color: #07ed07">Mastodon</span>


<!-- .slide: data-background="./images/degooglisons-internet.jpg" data-background-size="90%" -->
### Des services alternatifs

Framasoft via l'initiative Dégooglisons Internet propose plein de services

<https://degooglisons-internet.org>


<!-- .slide: data-background="./images/CC-logo.svg" data-background-size="70%" data-background-color="#bbb" -->
### Et au-delà du logiciel ?

Car oui, il y a aussi du numérique libre !


<!-- .slide: data-background="./images/Wikipedia.svg" data-background-size="50%" -->
### Wikipédia

L'encyclopédie collaborative.

<sub>Et ses petits frères : le wiktionnaire, wikisource, wiki commons, … </sub>


### Rechercher des créations libres ?

<https://search.creativecommons.org/>

<https://ccsearch.creativecommons.org>
